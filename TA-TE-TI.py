#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 17:09:26 2020

@author: ivnav

El juego es llamado con la operacion juego()

cada vez que se llama el tablero es creado en blanco, con elementos 'guion bajo'
representando asi los espacios vacios.

Las elecciones de la maquina son aleatorias, y no contienen ninguna clase de estrategias.

La eleccion de filas y columnas se hacen desde el 1 al 3. No sigue la forma de "indexacion" propia de Python.
Esto ultimo con fin de ser mas sencillo para un usuario que lo desconozca.
"""
import random



def crea_tablero():
    global tablero
    global victoria
    victoria = 0
    tablero =[['_','_','_'],
              ['_','_','_'],
              ['_','_','_']]

    [print(tablero[fila]) for fila in range(3)]


def eleccion():
#    x=0
    global inicia_x 
    while True:
        
        print('¿Desea jugar con "x" o con "o" - jugador "x" juega primero')
        respuesta = input()
        if (respuesta == 'X') or (respuesta == 'x'): 
            inicia_x = 1
            print('Jugador usa las X y juega primero')
            return inicia_x
        elif (respuesta == 'O') or (respuesta == 'o'):   
            inicia_x = 2
            print('Jugador usa las "O" y juega segundo')
            return inicia_x
        else:
            print('Favor seleccione "x" u "o"' )
            pass
        
def jugada_aleatoria(primero_juega):
    posibilidades = list()
    m1=[i for i,x in enumerate(tablero[0]) if x=='_']
    m2=[i for i,x in enumerate(tablero[1]) if x=='_']
    m3=[i for i,x in enumerate(tablero[2]) if x=='_']
    
    while set(m1+m2+m3) != set():
        if m1 != []:
            (p1,f1)=(0,random.choice(m1))
            posibilidades.append([p1,f1])
            m1 = []
        elif m2 != []:
            (p2,f2) = (1,random.choice(m2))
            posibilidades.append([p2,f2])
            m2 = []
        elif m3 != []:
            (p3,f3) = (2,random.choice(m3))
            posibilidades.append([p3,f3])
            m3 = []
        
    coordenada_aleatoria = random.choice(posibilidades)
        
    (x,y)=tuple(coordenada_aleatoria)
    if primero_juega == 1:    
        tablero[x][y]='O'
    else: 
        tablero[x][y]= 'X'
    print(tablero[0]);print(tablero[1]);print(tablero[2])
#    random.choice[m1,m2,m3]
#    posibilidad1 = []
#    possibilidades = 
#    if primero_juega == 1:
#
#    elif x==2:
#        
#    return board        
        
        
        
        
        
def check_filas():
    global victoria
    fila1 = tablero[0]
    fila2 = tablero[1]
    fila3 = tablero[2]        
    if (set(fila1)=={'X'}) or (set(fila2)=={'X'}) or (set(fila3)=={'X'}):
        print('Jugador "X" ha ganado')
        victoria = 1
        return victoria
    elif (set(fila1)=={'O'}) or (set(fila2)=={'O'}) or (set(fila3)=={'O'}):
        print('Jugador "O" ha ganado')
        victoria =2
        return victoria
#    elif set(fila1+fila2+fila3) == {'_'}:
#        victoria = 3
#        print('Juego Empatado')
#        return victoria

def check_columnas():
    global victoria
    columna1 = [tablero[i][0] for i in range(3)]
    columna2 = [tablero[i][1] for i in range(3)]
    columna3 = [tablero[i][2] for i in range(3)]
    if (set(columna1)=={'X'}) or (set(columna2)=={'X'}) or (set(columna3)=={'X'}):
        print('Jugador "X" ha ganado')
        victoria = 1
        return victoria
    elif (set(columna1)=={'O'}) or (set(columna2)=={'O'}) or (set(columna3)=={'O'}):
        print('Jugador "O" ha ganado')
        victoria = 2
        return victoria
        
        
def check_diagonales():
    global victoria
    diagonal1 = [tablero[0][0],tablero[1][1],tablero[2][2]]
    diagonal2 = [tablero[0][2],tablero[1][1],tablero[2][0]] 
    if (set(diagonal1)=={'X'}) or (set(diagonal2)=={'X'}):
        print('Jugador "X" ha ganado')
        victoria = 1
        return victoria
    elif (set(diagonal1)=={'O'}) or (set(diagonal2)=={'O'}):
        print('Jugador "O" ha ganado')
        victoria = 2
        return victoria
    
def juego():
    crea_tablero()
    eleccion()
    juega_maquina = True
    primera_maquina = 0
    if inicia_x == 2 and primera_maquina == 0:
        jugada_aleatoria(inicia_x)
        primera_maquina = 1
        
    while victoria == 0:
       
        print('¿En que coordenada desea jugar? Favor seleccionar numeros entre 1 y 3')
        print('Fila numero:')
        f = input()
        print('Columna numero:')
        c = input()
        try:
            coordenadaf = int(f)
            coordenadac = int(c)
            
            if (0<coordenadaf<4) and (0<coordenadac<4):
                if tablero[coordenadaf-1][coordenadac-1] != '_':
                    print('Ocupada, intente otra ubicacion')
                    juega_maquina = False
                    pass
                else:
                    if inicia_x == 1:
                        tablero[coordenadaf-1][coordenadac-1] = 'X'
                        print(tablero[0]);print(tablero[1]);print(tablero[2])
                        
                        
                    elif inicia_x == 2:
                        
                        
                        tablero[coordenadaf-1][coordenadac-1] = 'O'
                        print(tablero[0]);print(tablero[1]);print(tablero[2])
                        
            else:
                print('Favor ingrese valor de coordenadas entre 1 y 3')
                juega_maquina = False
        
        except ValueError:
            print('excepcion')
        
        else:
            check_filas()
            check_columnas()
            check_diagonales()
            
            if victoria != 0:
                break
            elif set(tablero[0]+tablero[1]+tablero[2])== {'X','O'}:
                print('Juego empatado')
                break
            if juega_maquina == True:     
                print('Jugada maquina: ')
                jugada_aleatoria(inicia_x)
                
            juega_maquina = True
            check_filas()
            check_columnas()
            check_diagonales()
            if victoria != 0:
                break
            elif set(tablero[0]+tablero[1]+tablero[2])== {'X','O'}:
                print('Juego empatado')
                break
            pass
            
juego()